# Use AWS Provider
provider "aws" {
  region = var.region
}

# Set S3 backend for terraform state files
terraform {
  backend "s3" {
    bucket = "cw-sb-sit-s3-tf-01"
    key    = "sit/terraform.tfstate"
    region = "ap-southeast-1"
  }
}

# Test creating a VPC
resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
  tags = {
      Name = "cw-sb-vpc-001"
      owner = "chee wei"
  }
}

# Test csv input
locals {
  subnets = csvdecode(file("../../csv/sit/subnets.csv"))
}

resource "aws_subnet" "test_subnets_csv" {
  for_each = { for subnet in local.subnets : subnet.subnet_name => subnet}

    vpc_id = aws_vpc.main.id
    cidr_block = each.value.cidr_block
    availability_zone = each.value.availability_zone
    tags = {
        Name = each.value.subnet_name
    }
}


// Comment to kick-start git push
// Comment to kick-start git push