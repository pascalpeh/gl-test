# Use AWS Provider
provider "aws" {
  region = var.region
}

# Set S3 backend for terraform state files
terraform {
  backend "s3" {
    bucket = "cw-sb-s3-prd-tf-02"
    key    = "prd/terraform.tfstate"
    region = "ap-northeast-1"
  }
}

 # Test creating a VPC
 resource "aws_vpc" "main" {
   cidr_block = "10.0.0.0/16"
   tags = {
       Name = "cw-sb-vpc-001"
       owner = "chee wei"
   }
 }

# Test csv input
locals {
  subnets = csvdecode(file("../../csv/prd/subnets.csv"))
}

resource "aws_subnet" "test_subnets_csv" {
  for_each = { for subnet in local.subnets : subnet.subnet_name => subnet}

    vpc_id = aws_vpc.main.id
    cidr_block = each.value.cidr_block
    availability_zone = each.value.availability_zone
    tags = {
        Name = each.value.subnet_name
    }
}

// Create SSM Parameters
resource "aws_ssm_parameter" "adpw" {
  name  = "adpw"
  type  = "SecureString"
  value = "P@ssw0rd123"
}

data "aws_vpc" "selected" {
  tags = {
    Name = "cw-sb-vpc-001"
  } 
}

output vpc_id {
  value       = data.aws_vpc.selected
}


// # Test importing a VPC
// resource "aws_vpc" "import_test" {
//   cidr_block = "10.10.10.0/24"
//   tags = {
//       Name = "cw-sb-vpc-002"
//       owner = "chee wei"
//   }
// }

// Comment to kick-start git push
// Comment to kick-start git push